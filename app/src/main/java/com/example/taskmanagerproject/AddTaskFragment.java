package com.example.taskmanagerproject;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.taskmanagerproject.databinding.FragmentAddTaskBinding;

public class AddTaskFragment extends Fragment {

    private AddTaskViewModel viewModel;
    private EditText taskDescriptionEditText;
    private Button addTaskButton;

    private FragmentAddTaskBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentAddTaskBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        return view;
    }

    addTaskButton = binding.addTaskButton;;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
